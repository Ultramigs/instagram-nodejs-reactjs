const mongoose = require('mongoose');

const PostSchema = mongoose.Schema({
    author: String,
    place: String,
    description: String,
    hashtags: String,
    comments: [{
        user: String,
        comment: String
    }],
    image: String,
    likes: {
        type: Number,
        default: 0,
    },
}, {
    timestamps: true,
});

module.exports = mongoose.model('Post', PostSchema);