const Post = require('../models/Post');

module.exports = {
    async store(req, res) {
        const post = await Post.findById(req.params.id);

        // const { comment } = req.body;
        post.comments.push(req.body);
        await post.updateOne(post);

        req.io.emit('post', post);

        return res.json(post);
    },

    async destroy (req, res){
        const post = await Post.findById(req.params.id);

        post.comments.id(req.params.idComments).remove();
        await post.save();
             
        req.io.emit('post', post);

        return res.json(post);
    }
}