import React from 'react';
import { Link } from 'react-router-dom';

import './Header.css'
import logo from '../Assets/logo.svg';
import camera from '../Assets/camera.svg';

export default function Header() {
  return (
    <header id="main-header" className="main-header">
        <div className="header-content">
            <Link to="/"> 
              <img className="icon-size" src={logo} alt="InstaRocket"/>
            </Link>
            <Link to="/new">
              <img className="icon-size" src={camera} alt="Enviar Publicação"/>
            </Link>
        </div>
    </header>
  );
}
