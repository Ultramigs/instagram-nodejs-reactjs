import React, { Component } from 'react';
import api from '../services/api';
import io from 'socket.io-client';

import './Feed.css';

import more from '../Assets/more.svg';
import like from '../Assets/like.svg';
import comment from '../Assets/comment.svg';
import send from '../Assets/send.svg';
import user from '../Assets/user.svg';


class Feed extends Component {
    state = {
        feed: [],
    };

    async componentDidMount(){
        this.registerToSocket();

        try{
            const response = await api.get('posts');
            this.setState({feed: response.data});
        } catch (error){
            debugger;
        }
        
     
    }

    registerToSocket = () => {
        const socket = io('http://localhost:3333');

        socket.on('post', newPost => {
            this.setState({
                feed:[newPost, ...this.state.feed]
            })
        })

        socket.on('like', likedPost => {
            this.setState({
                feed: this.state.feed.map(post => 
                post._id === likedPost._id ? likedPost : post
                    )
            })
        })
    }

    handleLike = id => {
        api.post(`/posts/${id}/like`);
    }

    render(){
        return (
           <section id="post-list" className="post-list">
              {this.state.feed.map(post => ( 
                <article key={post._id} className="post-item">
                <header>
                    <div className="user-header">
                        <div className="user-picture-container">
                            <img src={user} className="user-picture" alt=""/>
                        </div>
                        <div className="user-info">
                            <span className="user">{post.author}</span>
                            <span className="place">{post.place}</span>
                        </div>
                    </div>
                        <img className="icon-size  plus" src={more} alt="Mais"/>
                </header>
                <img className="post-image" src={`http://localhost:3333/files/${post.image}`} alt="Mais"/>

                <footer className="post-footer">
                <div className="actions">

                    <img onClick={() => this.handleLike(post._id)} className="icon-size like-button" src={like} alt="Mais"/>
                    <img className="icon-size" src={comment} alt="Mais"/>
                    <img className="icon-size" src={send} alt="Mais"/>
                </div>

                <strong>{post.likes} curtidas</strong>

                <p className="post-description">
                    {post.description}

                    <span className="post-hashtags">{post.hashtags}</span>
                </p>
                
                <p className="comment-section">

                {
                    post.comments.map(comment => (
                    <span key={comment._id} className="comment-item">
                       <span className="comment-item_user"> {comment.user} </span>
                       <span className="comment-item_comment"> {comment.comment} </span>
                    </span>
                ))} 

                </p>
                </footer>
            </article>

                ))} 
          
           </section>
        );
    }
}

export default Feed;